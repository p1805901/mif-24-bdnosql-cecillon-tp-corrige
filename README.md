# Page de l'UE MIF24 Bases de Données non relationnelles

[Master Informatique](http://master-info.univ-lyon1.fr/M1/) de l'Université Lyon 1.

## Année 2022-2023

Intervenants: [Emmanuel Coquery](http://emmanuel.coquery.pages.univ-lyon1.fr/) et [Fabien Duchateau](https://perso.liris.cnrs.fr/fabien.duchateau/)

Évaluation: 60% QCMs au début de chaque cours (sauf le premier), 40% ECA

### Séances et supports

| Séance | Type | Contenu                         | Supports                                                                                   |
| ------ | ---- | ------------------------------- | ------------------------------------------------------------------------------------------ |
| 09/09  | CM   | Chemins                         | [Diapositives](cm/cm1-chemins.pdf)                                                         |
|        | TD   | Chemins                         | [Sujet corrigé](td/td1-chemins.pdf), [code exercice 3](td/td1-exo3/)                       |
| 16/09  | CM   | Patterns de documents           | [Diapositives](cm/cm2-sgbd-document.pdf)                                                   |
|        | TP   | Chemins (JSON, sous PostgreSQL) | [Sujet](tp/tp1-chemins.md), [corrigé](tp/tp1-corrige.sql)                                  |
| 07/10  | CM   | JSON schema, types algèbre      | [Diapositives](cm/cm3-types-json-schema.pdf)                                               |
|        | TD   | JSON schema, types algèbre      | [Sujet corrigé](td/td2-types.pdf)                                                          |
| 14/10  | CM   | Algèbre                         | [Diapositives](cm/cm4-algebres-collections.pdf)                                            |
|        | TD   | Algèbre                         | [Sujet corrigé](td/td3-algebre.pdf)                                                        |
| 04/11  | TP   | Requêtes Mongo                  | [Sujet](tp/tp2-aggregation-pipeline.md)                                                    |
|        | TP   | Requêtes Mongo (suite)          |                                                                                            |
|        | CM   | Code algèbre                    | [Diapositives](cm/cm4-algebres-collections.pdf) (mêmes diapos que le CM Algèbre)           |
| 02/12  | CM   | RDF SPARQL                      | [Diapositives](cm/cm6-rdf-sparql.pdf)                                                      |
|        | TD   | RDF SPARQL                      | [Sujet corrigé](td/td4-rdf-sparql.pdf) (c'est le td2 de l'UE de l'an dernier)              |
| 09/12  | CM   | RDF SHACL                       | [Diapositives](cm/cm7-shacl.pdf)                                                           |
|        | TD   | RDF SHACL                       | [Sujet](td/td5-shacl.pdf)                                                                  |
|        | TP   | RDF SPARQL                      | [Sujet](tp/tp3-sparql.md)                                                                  |
| 13/01  | CM   | ORM                             | [Diapositives](cm/cm8-orm.pdf)                                                             |
|        | TP   | ORM                             | [Sujet](https://forge.univ-lyon1.fr/mif24-bdnosql/mif24-bdnosql-orm/-/blob/main/README.md) |
| 20/01  | TP   | ORM                             | Suite de la semaine précédente                                                             |
|        | TD   | Révisions                     |            [Sujet](td/td6-revisions.pdf)                                              |
| 27/01  | ECA  |                                 |                                                                                            |


### Sujets d'examen

- [sujet 2021](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2021-2022-s1.pdf), avec [son corrigé](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2021-2022-s1-corrige) et des [explications en vidéo](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.3/514be0833f3339fc1b5110968f76d29369c161c5-1644316265552)
- [sujet 2020](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2020-2021-s1.pdf)
- [sujet 2019](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2019-2020-s1.pdf)
- [sujet 2018](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2018-2019-s1.pdf)