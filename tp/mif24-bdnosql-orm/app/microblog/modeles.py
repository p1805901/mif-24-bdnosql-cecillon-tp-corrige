from microblog.database import db


class User(db.Model):
    __tablename__ = "users"
    email = db.Column(db.String, primary_key=True)
    pseudo = db.Column(db.String)


class Categorie:
    def __init__(self) -> None:
        __tablename__ = 'categorie'
        self.nom = db.Column(db.String, unique=True, nullable=False)
        self.billets = []


class Billet:
    def __init__(self) -> None:
        self.titre = None
        self.contenu = None
        self.categorie = None
        self.user = None
