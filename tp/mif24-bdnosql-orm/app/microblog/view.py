from urllib.parse import quote

USER_EMAIL = "user_email"
CATEGORIES = "categories"
BILLET = "billet"
MAIN_VIEW = "main_view"
EMPTY_V = "empty"
BILLET_V = "billet"
CREATE_V = "create"
EDIT_V = "edit"
MESSAGE = "message"


def main_view(view_context):
    """Génère la page HTML

    Args:
        view_context (dict): données pour le rendu

    Returns:
        str: code html
    """
    return (
        render_header(view_context)
        + render_login(view_context)
        + render_categories(view_context)
        + render_main(view_context)
        + render_footer(view_context)
    )


def render_header(view_context):
    """Génère le header de la page"""
    return """
<html>
  <head>
    <title>{title}</title>
  </head>
  <body>
    <h1><a href="/">{title}</a></h1>
""".format(
        **view_context
    )


CREATE_BUTTON_TPL = """<div><form action="/" method="get">
    <input type="submit" value="Créer un billet">
    <input type="hidden" name="action" value="create">
</form></div>"""
FOOTER_TPL = """
  </body>
</html>
"""


def render_footer(view_context):
    """Génère le base de page"""
    create_btn = ""
    if USER_EMAIL in view_context:
        create_btn = CREATE_BUTTON_TPL
    return create_btn + FOOTER_TPL


def render_login(view_context):
    """Génère un formulaire de login si l'utilisateur n'est pas connecté ou un formulaire de déconnexion dans le cas contraire"""
    if USER_EMAIL in view_context and view_context[USER_EMAIL] is not None:
        form = """<form action="/logout" method="post">
    <p><input type="submit" value="Logout"><input type="hidden" name="action" value="logout"></p>
</form>"""
    else:
        form = """<form action="/login" method="post">
    <p>
        Connexion: email <input type="text" name="email">
        <input type="submit"><input type="hidden" name="action" value="login">
    </p>
</form>
<form action="/register" method="post">
    <p>
        Création de compte: email <input type="text" name="email"> surnom <input type="text" name="pseudo">
        <input type="submit">
        <input type="hidden" name="action" value="create_user">
    </p>
</form>"""
    return """
    <div class="login">
    {form}
    </div>
    """.format(
        form=form, **view_context
    )


BILLET_ITEM_TPL = """
<li><a href="?titre={titreEncode}&categorie={categorieEncode}">{titre}</a></li>
"""


def render_billet_item(billet):
    return BILLET_ITEM_TPL.format(
        titreEncode=quote(billet.titre),
        categorieEncode=quote(billet.categorie.nom),
        titre=billet.titre,
    )


CATEGORIE_TPL = """
<dt>{titre}</dt>
<dd>
    <ul>
        {billets}
    </ul>
</dd>
"""


def render_categorie(categorie):
    return CATEGORIE_TPL.format(
        titre=categorie.nom, billets="".join(map(render_billet_item, categorie.billets))
    )


CATEGORIES_TPL = """<div class="categories" style="float:right"><h3>Billets</h3>
<dl>
  {html_cats}
</dl>
</div>
"""


def render_categories(view_context):
    categories = []
    if CATEGORIES in view_context:
        categories = view_context[CATEGORIES]
    return CATEGORIES_TPL.format(html_cats="".join(map(render_categorie, categories)))


BILLET_TPL = """<div class="main"><h2>{titre}</h2>
    <p>{pseudo}</p>
    <div>{contenu}</div>
</div>
"""

EDIT_BTN_TPL = """<div><form action="/" method="get">
    <input type="submit" value="Modifier ce billet">
    <input type="hidden" name="action" value="edit">
    <input type="hidden" name="titre" value="{titre}">
    <input type="hidden" name="categorie" value="{categorie}">
</form></div>"""


def render_billet(view_context):
    billet = view_context[BILLET]
    user_email = view_context[USER_EMAIL]
    rendered = BILLET_TPL.format(
        titre=billet.titre, pseudo=billet.user.pseudo, contenu=billet.contenu
    )
    if user_email is not None and user_email == billet.user.email:
        rendered = rendered + EDIT_BTN_TPL.format(
            titre=billet.titre, categorie=billet.categorie.nom
        )
    return rendered


CREATE_TPL = """<div class="create">
<form action="/create" method="post">
    <p>
        Titre: <input name="titre" type="text"><br/>
        Categorie: <input name="categorie" type="text"><br/>
        <textarea name="contenu"></textarea>
        <input type="submit">
        <input type="hidden" name="action" value="create">
        <input type="hidden" name="user" value="{email}">
    </p>
</form>
</div>"""


def render_create(view_context):
    return CREATE_TPL.format(email=view_context[USER_EMAIL])


EDIT_TPL = """<form action="/edit" method="post">
    <h3>Modifier</h3>
    <textarea name="contenu">{contenu}</textarea>
    <input type="submit">
    <input type="hidden" name="action" value="edit">
    <input type="hidden" name="titre" value="{titre}">
    <input type="hidden" name="categorie" value="{categorie}">
    <input type="hidden" name="user" value="{email}">
</form>"""


def render_edit(view_context):
    billet = view_context[BILLET]
    return EDIT_TPL.format(
        contenu=billet.contenu,
        titre=billet.titre,
        categorie=billet.categorie.nom,
        email=billet.user.email,
    )


def render_main(view_context):
    mv = ""
    if MAIN_VIEW in view_context:
        mv = view_context[MAIN_VIEW]
    if mv == BILLET_V:
        return render_billet(view_context)
    elif mv == CREATE_V:
        return render_create(view_context)
    elif mv == EDIT_V:
        return render_edit(view_context)
    else:
        return ""
