from flask import session, request, redirect, url_for, Flask

from microblog.view import main_view, USER_EMAIL, MESSAGE, MAIN_VIEW, BILLET, BILLET_V
from microblog.requetes import *


def check_user(view_context):
    if USER_EMAIL in session and session[USER_EMAIL] is not None:
        view_context[USER_EMAIL] = session[USER_EMAIL]
    if MESSAGE in session:
        view_context[MESSAGE] = session[MESSAGE]
        session.pop(MESSAGE, "")


def register_routes(app: Flask):
    @app.route("/")
    def index():
        view_context = {"title": "Microblog", "categories": get_all_categories()}
        check_user(view_context)
        titre = request.args.get("titre", "").strip()
        categorie = request.args.get("categorie", "").strip()
        if len(titre) > 0 and len(categorie) > 0:
            billet = get_billet(titre, categorie)
            if billet is not None:
                view_context[BILLET] = billet
        action = request.args.get("action", "").strip()
        if len(action) > 0:
            view_context[MAIN_VIEW] = action
        elif BILLET in view_context:
            view_context[MAIN_VIEW] = BILLET_V
        return main_view(view_context)

    @app.route("/login", methods=["POST"])
    def login_r():
        email = request.form.get("email", "").strip()
        if len(email) > 0:
            user = get_user_by_email(email)
            if user is not None:
                session[USER_EMAIL] = user.email
                session[MESSAGE] = "Connexion réussie"
            else:
                session.pop(USER_EMAIL, "")
                session[MESSAGE] = "Utilisateur " + email + " inconnu"
        return redirect(url_for("index"))

    @app.route("/logout", methods=["POST"])
    def logout_r():
        session.pop(USER_EMAIL, "")
        session[MESSAGE] = "Déconnexion réussie"
        return redirect(url_for("index"))

    @app.route("/register", methods=["POST"])
    def register_r():
        email = request.form.get("email", "").strip()
        pseudo = request.form.get("pseudo", "").strip()
        if len(email) > 0:
            user = create_user(email, pseudo)
            if user is not None:
                session[USER_EMAIL] = user.email
                session[MESSAGE] = "Enregistrement réussi, connexion réussie"
            else:
                session[MESSAGE] = "Enregistrement échoué"
        return redirect(url_for("index"))

    @app.route("/create", methods=["POST"])
    def create_r():
        view_context = {"title": "Creation billet"}
        check_user(view_context)
        if USER_EMAIL not in view_context:
            return "Connexion nécéssaire"
        titre = request.form.get("titre", "").strip()
        categorie = request.form.get("categorie", "").strip()
        contenu = request.form.get("contenu", "").strip()
        billet = create_billet(titre, contenu, categorie, view_context[USER_EMAIL])
        if billet is None:
            return "erreur"
        return redirect(
            url_for("index", titre=billet.titre, categorie=billet.categorie.nom)
        )

    @app.route("/edit", methods=["POST"])
    def edit_r():
        view_context = {"title": "Creation billet"}
        check_user(view_context)
        if USER_EMAIL not in view_context:
            return "Connexion nécéssaire"
        titre = request.form.get("titre", "").strip()
        categorie = request.form.get("categorie", "").strip()
        contenu = request.form.get("contenu", "").strip()
        billet = get_billet(titre, categorie)
        if billet is None:
            return "erreur: billet inexistant"
        if billet.user.email != view_context[USER_EMAIL]:
            return "erreur: modification interdite"
        billet = update_billet(titre, categorie, contenu)
        if billet is None:
            return "erreur à la modification du billet"
        return redirect(
            url_for("index", titre=billet.titre, categorie=billet.categorie.nom)
        )
