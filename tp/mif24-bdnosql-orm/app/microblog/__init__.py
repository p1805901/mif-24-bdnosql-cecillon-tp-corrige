from os import environ
from urllib.parse import quote_plus

from flask import Flask

from microblog.database import db, mk_db_uri
from microblog.blog import register_routes


def create_app():
    app = Flask(__name__)
    app.secret_key = b"ejah9nai1iCh0ohp"
    app.config["SQLALCHEMY_DATABASE_URI"] = mk_db_uri()
    db.init_app(app)
    register_routes(app)
    return app
