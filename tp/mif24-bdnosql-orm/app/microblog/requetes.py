from microblog.modeles import db, User, Categorie, Billet
from sqlalchemy import select
from sqlalchemy.orm import Session

# Contantes servant de bouche-trou en attendant l'implémentation des fonction de requêtes

USER_B = User()
USER_B.email = "rien_u@rien.org"
USER_B.pseudo = "rien_u"

CATEGORIE_B = Categorie()
CATEGORIE_B.billets = []
CATEGORIE_B.nom = "rien_c"

BILLET_B = Billet()
BILLET_B.titre = "rien_b"
BILLET_B.contenu = "rien_b_c"
BILLET_B.user = USER_B
BILLET_B.categorie = CATEGORIE_B

CATEGORIE_B.billets.append(BILLET_B)

# Attention, on est dans flask-sqlalchemy, donc on utilisera db.session
# et pas un objet session créé pour l'occasion


def get_user_by_email(email):
    user_corresponding = db.session.query(
        User).filter(User.email == email).first()
    return user_corresponding


def create_user(email, pseudo):
    user = User(
        email=email,
        pseudo=pseudo
    )
    db.session.add(user)
    db.session.commit()
    return user


def get_all_categories():
    db.session.query(Categorie).
    return [CATEGORIE_B]


def get_or_create_categorie(nom):

    return CATEGORIE_B


def get_billet(titre, categorie):
    db.session.query(Billet)
    return BILLET_B


def create_billet(titre, contenu, nom_categorie, email_utilisateur):
    # TODO: faire l'insersion en base
    return BILLET_B  # remplacer par le billet créé


def update_billet(titre, nom_categorie, nouveau_contenu):
    # TODO: faire la modification en base
    return BILLET_B  # remplacer par le billet modifié
