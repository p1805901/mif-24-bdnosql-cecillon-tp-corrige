"""
Module contenant la gestion de la connexion à la base de données.

Il est inutile de modifier ce code.
"""
from os import environ
from urllib.parse import quote_plus

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def mk_db_uri(host=None, user=None, password=None, dbname=None):
    if host is None:
        host = environ.get("PG_HOST")
    if user is None:
        user = environ.get("PG_USER")
    if password is None:
        password = environ.get("PG_PASSWORD")
    if dbname is None and user is not None:
        dbname = environ.get("PG_DB")
    url = "postgresql://"
    if user is not None and password is not None:
        url = url + quote_plus(user) + ":" + quote_plus(password) + "@"
    elif user is not None:
        url = url + quote_plus(user) + "@"
    url = url + host
    if dbname is not None:
        url = url + "/" + dbname
    return url
