from pytest import fixture
from flask import Flask
import sqlalchemy
from sqlalchemy import delete, func

from microblog import create_app
from microblog.modeles import db, User, Categorie, Billet
from microblog.requetes import *


@fixture
def app():
    yield create_app()


@fixture
def app_ctx(app: Flask):
    with app.app_context():
        yield


@fixture
def user1(app_ctx):
    db.session.query(User).filter(User.email == "user1@example.org").delete()
    db.session.commit()
    user = User(email="user1@example.org", pseudo="user1")
    db.session.add(user)
    db.session.commit()
    yield user
    db.session.delete(user)
    db.session.commit()


def test_get_user_by_email(app_ctx, user1):
    user = get_user_by_email(user1.email)
    assert user.email == user1.email
    assert user.pseudo == user1.pseudo
