# Lancer le conteneur docker pour la base de données locale

## Lancer le conteneur

A la racine du dossier de ce projet

```bash
docker-compose up
```

## Ajouter les tables à la base de données

Connectez-vous au conteneur à partir de votre terminal en saisissant la commande suivante:

```bash
docker exec -it mif24-bdnosql-orm-pg-1 psql -U postgres -d mif24
```

Coller les requêtes SQL suivantes:

```sql
DROP TABLE IF EXISTS BILLET;
DROP TABLE IF EXISTS CATEGORIE;
DROP TABLE IF EXISTS USERS;
CREATE TABLE CATEGORIE (
    ID_CAT SERIAL PRIMARY KEY,
    NOM VARCHAR(255) NOT NULL UNIQUE
);
CREATE TABLE USERS (
    EMAIL VARCHAR(255) PRIMARY KEY,
    PSEUDO VARCHAR(255)
);
CREATE TABLE BILLET (
    TITRE VARCHAR(255),
    ID_CAT INTEGER REFERENCES CATEGORIE (ID_CAT),
    USER_EMAIL VARCHAR(255) REFERENCES USERS (EMAIL),
    CONTENU TEXT,
    PRIMARY KEY (ID_CAT, TITRE)
);
```

## Vérifier que les tables sont bien créer

Tapez ou collez la commande suivante :

```sql
SHOW TABLES;
```

Si tout s'est bien déroulé vous devriez voir les tables BILLET, CATEGORIE, USERS

## Modification du fichier de configuration pour se connecter à la base de données locale

Si vous avez clonez ce projet cette partie n'est pas nécessaire sinon voici le code pour que votre application web se connecte à votre conteneur docker

```python
def mk_db_uri(host=None, user=None, password=None, dbname=None):
    if host is None:
        host = "localhost:54320"
    if user is None:
        user = "postgres" # Utilisateur de base dans la base de données
    if password is None:
        password = "dev-password" # Définie dans le docker-compose
    if dbname is None and user is not None:
        dbname = "mif24"
    url = "postgresql://" # Url de base.
    if user is not None and password is not None:
        url = url + quote_plus(user) + ":" + quote_plus(password) + "@"
    elif user is not None:
        url = url + quote_plus(user) + "@"
    url = url + host
    if dbname is not None:
        url = url + "/" + dbname # mif24
    return url
```

Si vous souhaitez avoir une vue d'ensemble des variables que l'on a défini et à quoi elles correspondent je vous invite à lire le début de cette doc qui explique comment connecter l'ORM Prisma à une base de données POSTGRES : <https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/connect-your-database-typescript-postgres>

Il est également possible de créer un docker compose pour votre application python - flash mais pour le moment ce n'est pas prévu.

## Happy coding
