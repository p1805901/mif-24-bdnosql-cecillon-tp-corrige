# TP ORM

Lisez tout l'énoncé avant de commencer.

## Objectif

Prendre en main un ORM (ici [SQLAlchemy][sqlalchemy]) dans ses
dimensions mapping (via la déclaration des classes correspondant aux tables),
mises à jour et requêtes.

## Contexte applicatif

On considère pour ce TP un site de blog minimaliste avec des utilisateurs, des
catégories et des billets.

### Base de données

Dans ce TP, le schéma de la base est imposé. Même si les ORM sont en général
capables de générer les tables, c'est un contexte de travail assez réaliste en
pratique, les équipe de développement aimant garder une trace des schéma en
version SQL et gérer les migrations de données lors des mises à jour des
applications avec des outils comme [liquibase].

On travaillera sur la base de donnée postgres déjà utilisée pour le TP SQL/JSON.

On créera le schéma dans la base de données à l'aide du script
[microblog.sql](microblog.sql).

### Application de départ

L'application fournie est réalisée en Python en utilisant le framework
[Flask][flask] et son extension [Flask-SQLAlchemy][flask-sqlalchemy] (qui
utilise elle-même [SQLAlchemy][sqlalchemy]).

Le code source de l'application se trouve dans le répertoire [microblog](microblog).
Il est composé des fichiers suivants (qui a priori n'ont pas besoin d'être modifiés):

- `__init__.py`: c'est le point de départ de l'application.
- `database.py`: comporte le nécessaire pour mettre en place le framework
  [SQLAlchemy] via [Flask-SQLalchemy].
- `blog.py`: contient le code métier (gestion des billets et des utilisateurs)
  et les différentes routes (configuration des urls) associées.
- `view.py`: contient le code qui va générer le contenu HTML.

Les fichiers suivants devront être complétés et/ou modifiés par vos soins:

- `modeles.py`: contient les définitions des classes qui doivent correspondre
  aux tables. Le mapping de la table `users` avec la classe `User` a déjà été
  réalisé à titre d'exemple.
- `requetes.py`: contient les fonctions d'accès aux données utilisées par
  `blog.py`.
- `tests/test_blog.py` contient la mécanique pour tester les fonction de
  `requetes.py`, ainsi qu'un exemple de test pour la fonction
  `get_billet_by_email`. Le framework de test utilisé est [pytest]. Il est
  conseillé de consulter la documentation sur les fixtures de [pytest] afin de
  bien appréhender le code de ce fichier. Vous pourrez y ajouter vos propres
  test, pour tester les fonctions à compléter dans `requetes.py`, mais aussi
  pour tester le comportement de [SQLAlchemy] si la documentation n'est pas
  claire pour vous.

### Quelques mots à propos de Flask

Il y a quelques différences dans l'usage de [SQLAlchemy] au sein de [Flask] par
rapport à ce qui est décrit dans la documentation de [SQLAlchemy]:

- la classe `Base` est remplacée par `db.Model` où `db` est définie dans le
  module `microblog.database`.
- la session est gérée par [Flask] et est accessible par `db.session`. On pourra
  par exemple utiliser `db.session.get()` pour récupérer un objet via sa clé
  primaire ou bien `db.session.execute(stmt)` pour exécuter la requête placée
  dans `stmt`.
- de même la plupart des classes et fonctions de SQLAlchemy sont accessibles via
  `db` plutôt que via un `import` Python classique.

## Lancement de l'application et des tests

### Environnement

L'application est prévue pour récupérer les informations de connexion à la base
de données depuis des variables d'environnement, plus précisement:

- `PGPASSWORD` doit contenir votre mot de passe pour la base de données
  PostgreSQL (celui fourni via [Tomuss]).
- `PG_USER` peut contenir au autre utilisateur que votre login, par exemple
  celui de votre binôme (attention à adapter `PGPASSWORD` mot de passe précédent
  dans ce cas). Par défaut c'est votre login étudiant qui sera utilisé.

### Environnement virtuel Python

Il est nécessaire de créer un environnement virtuel Python pour lancer
l'application. Cela peut se faire via la commande suivante (pour créer
l'environnement dans le répertoire `.venv`):

```
python3 -m venv .venv
```

pour activer cet environnement dans votre shell (en bash):

```
source .venv/bin/activate
```

Si vous créez cet environnement dans le répertoire du projet, VSCode devrait le
détecter et vous le proposer comme environnement Python pour votre projet.

Il faut installer les dépendances du projet, dans un shell où l'environnement
virtuel est activé, faire:

```
pip install -r requirements.txt
```

### Lancement de l'application et des tests

Les commandes suivantes sont à lancer depuis la racine du projet avec
l'environnement virtuel activé.

L'application peut être lancée via la commande

```
flask --app microblog --debug run
```

Les tests sont à lancer via la commande:

```
python3 -m pytest
```

## Les classes et fonctions utiles pour compléter le code

- [relationship](https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html)
  permet de définir une association entre deux entités.
- [db.Column](https://docs.sqlalchemy.org/en/14/core/metadata.html#sqlalchemy.schema.Column)
  pour définir un mapping champ-colonne. En particulier la méthode `__init__`.
- db.String, db.Integer pour spécifier le type des colonnes.
- [db.ForeignKey](https://docs.sqlalchemy.org/en/14/core/constraints.html#sqlalchemy.schema.ForeignKey)
  à utiliser dans une colonne pour déclarer une clé étrangère
- [d.session](https://docs.sqlalchemy.org/en/14/orm/session_api.html#sqlalchemy.orm.Session)
  pour ajouter, supprimer, récupérer des objets, faire des commit.

## Travail à faire

Ce TP n'est _pas_ à rendre, mais des questions seront posées dessus à l'examen.
Il est donc conseillé de faire un petit compte-rendu complémentaire au code pour faciliter les révisions.

1. Faire un fork de ce projet et le partager avec votre binôme.
2. Configurer votre environnement de travail (variables d'environnement, environnement virtuel python, configuration VSCode ou autre IDE)
3. Créer les tables dans votre compte PostgreSQL.
4. Parcourir le code de l'application, en particulier `modeles.py` et `requetes.py`
5. Lancer les tests pour vérifier la configuration. Le test doit échouer avec une erreur comme ci-dessous. Une autre erreur est symptomatique d'un problème de configuration.

```
FAILED tests/test_blog.py::test_get_user_by_email - AssertionError: assert 'rien_u@rien.org' == 'user1@example.org'
```

6. Lancer l'application et vérifier qu'elle fonctionne à l'adresse indiquée dans le log
7. Compléter les méthodes liées à la classe `User` dans `requetes.py`. Tester dans l'application et en ajoutant vos propres tests.
8. Modifier la classe `Categorie` et compléter les fonctions associées dans `requetes.py`. Pour le moment on affectera le champ billet à un tableau vide. Tester.
9. Modifier la classe `Billet` et compléter les fonctions associées dans `requetes.py`. Ajouter les associations Billet<->User et Billet<->Categorie. Remplacer définir correctement le champ `billets` dans la classe Categorie. Tester.

[sqlalchemy]: https://www.sqlalchemy.org/
[flask]: https://flask.palletsprojects.com/en/2.2.x/
[flask-sqlalchemy]: https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/
[pytest]: https://docs.pytest.org/en/7.2.x/
[liquibase]: https://www.liquibase.org/
[tomuss]: https://tomuss.univ-lyon1.fr
