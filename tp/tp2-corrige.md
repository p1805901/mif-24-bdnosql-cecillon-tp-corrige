# Premières requêtes

## 1. Pour chacune des collections grades et zip, récupérer un document, puis en déduire un type pour les éléments de cette collection

### Grades

```js
db.grades.findOne({class_id : 5});
```

```js
{ _id: ObjectId("50b59cd75bed76f46522c350"),
  student_id: 0,
  class_id: 5,
  scores:
   [ { type: 'exam', score: 88.22950674232497 },
     { type: 'quiz', score: 79.28962650427184 },
     { type: 'homework', score: 18.66254946562674 },
     { type: 'homework', score: 40.28154176513361 },
     { type: 'homework', score: 1.23735944117882 },
     { type: 'homework', score: 88.96101200683958 } ]
}
```

#### Types

| _id  | student_id  |  class_id | scores |
|---|---|---|---|
|  ObjectId | Int32  |  Int32 | Array |

- [ObjectId](<https://www.mongodb.com/docs/v6.0/reference/method/ObjectId/?_ga=2.213925079.1394125933.1667567932-1500272430.1667567932>)

### Zips

```js
db.zips.findOne({_id : "01001"})
```

#### Types

| _id  | city | loc | pop | state |
|---|---|---|---|---|
| String | String | Array | Int32 | String

## 2. Récupérer quelques documents de grades et vérifier qu'ils sont conformes au type de la question précédente

[Type](<https://www.mongodb.com/docs/manual/reference/operator/query/type/>)

### Grades

```js
db.grades.find({
  _id : { $type:  7},
  student_id: { $type: 16},
  class_id: {$type: 16},
  scores: {$type: 4}
});
```

### Zips

```js
db.zips.find({
  _id : { $type:  2},
  city : { $type:  2},
  loc : { $type: 4},
  pop : { $type : 16},
  state: { $type: 2 }
 });
```

## 3. Donner le nombre de résultats de la requête précédente (résultat attendu: 280)

### Grades

```mongo
db.grades.find({
  _id : { $type:  7},
  student_id: { $type: 16},
  class_id: {$type: 16},
  scores: {$type: 4}
}).count();
```

Ce n'est pas vraiment la réponse exact à la question, on voulait simplement avoir le bon nombre de documents dans la collection mais bon ça me semble être la plus acceptable.

### Zips

```mongo
db.zips.countDocuments();
```

Réponse accepté par le prof.

## 4. Récupérer les documents dont le class_id vaut 20 dans la collection grades (7 résultats)

### Grades

```js
db.grades.find({
  class_id : 20
});
```

On peut ajouter le `count()` pour vérifier que l'on a le bon nombre de documents.

## 5. Récupérer les documents dont le class_id est inférieur ou égal à 20. L'opérateur inférieur ou égal se note $lte. (188 résultats)

### Grades

```js
db.grades.find({
  class_id: {$lte : 20}
})
```

## 6. Récupérer les documents dont le class_id est inférieur ou égal à 20. L'opérateur inférieur ou égal se note $lte. (188 résultats)

### Grades

```js
db.grades.find({
  $expr : {
    $gte : ["$student_id", "$class_id"]
  }
});
```

## 7. Récupérer les documents dont le class_id est compris entre 10et 20 (100 résultats). Faire une version avec un double filtre sur le champ class_id (via la syntaxe { field: { op1: value1, op2: value2 } }), puis une autre version avec un $expr contenant un $and

### Grades

```js
db.grades.find({
  class_id : {
     $gte : 10,
     $lte : 20
  }
});
```

```js
db.grades.find({
  $expr : {
    $and: [
     { $gte : ["$class_id", 10] },
     { $lte : ["$class_id", 20] }
    ]
  }
});
```

## 8. Donner tous les documents de la collection en renommant le champ class_id en ue et le champ student_id en etu

### Grades

```js
db.grades.find({}, {ue: "$class_id", etu: "$student_id"}).count();
```

Autre façon de faire avec les aggregations

```js
db.grades.aggregate([
  {
    $project : {
      "ue" : "$class_id",
      "etu" : "$student_id"
    }
  }])
```

# Prise en main de quelques étapes d'agrégation

## 1. Donner tous les documents de la collection grades en y ajoutant un champ somme dont la valeur est la somme des champs class_id et student_id. on utilisera pour cela le pipeline d'agrégation avec un $project

```js
db.grades.aggregate([{
  $project : {
    "somme" : {
      $sum : {
        $add : [
          "$class_id", "$student_id"]
      }
    }
  }
}]);
```

<https://stackoverflow.com/questions/16676170/is-it-possible-to-sum-2-fields-in-mongodb-using-the-aggregation-framework>

## 2. Dans la collection zips récupérer les documents ayant une population supérieure ou égale à 10000 (7634 résultats)

```js
db.zips.aggregate([
  {
    $match : {
    pop: {$gte : 10000}
    }
  }
]);
```

## 3. Dans la collection grades, renvoyer les documents triés par class_id, puis par student_id (par ordre croissant pour les 2 champs)

```js
db.grades.aggregate([
  {
    $sort : {
    class_id: 1,
    student_id: 1
    }
  }
])
```

## 4. Dans la collection grades, produire un document pour chaque élément du tableau contenu dans le champ scores (1241 résultats)

```js
db.grades.aggregate([
  {$unwind : "$scores"}
  ])
```

## 5. Dans la collection zips, donner pour chaque ville la population minimale (16584 résultats)

```js
db.zips.aggregate([
  {
    $group : {
      _id: "$city",
      total: {
        $min: "$pop"
      }
    }
  }
  ]);
```

## 6. Effectuer une jointure entre grades et zips sur les champs student_id et pop. Quel est le type du résultat ?

```js
db.grades.aggregate([
  {$lookup : {from : "zips", localField : "student_id", foreignField: "pop", as: "jointure"}},
  {$limit : 2}])
```

Le limit est là pour que l'on voit un peu moins de données et voir le res de la jointure.

# Pipelines aggregate

## 1. Calculer le nombre de notes de chaque type

```js
db.grades.aggregate([
  {$unwind : "$scores"},
  {$group : {
    _id: "$scores.type",
    somme : {
      $sum : 1
    }
  }}
  ])
```

## 2. Donner pour chaque matière, la meilleure note d'examen.

```js
db.grades.aggregate([
  {$unwind: "$scores"},
  {
    $match: {
      "scores.type": {
        $eq: "exam"
      }
    }
  },
  {
    $group: {
      _id: "$class_id", 
      meilleur_note: {
        $max: "$scores.score"
      }
    }
  },
  {
    $sort: {
      "_id":1
    }
  }])
```

## 3. Donner les dix villes les plus peuplées. Indice: On pourra utiliser l'étape $limit.

```js
db.zips.aggregate([
  {
    $group: {
      _id: "$city", 
      pop_total: {
        $sum: "$pop"
      }
    }
  }, 
  {
    $sort: {
      pop_total:-1
    }
  }, 
  {
    $limit: 10
  }])
```


Vous pouvez soumettre une PR pour compléter 😀.
