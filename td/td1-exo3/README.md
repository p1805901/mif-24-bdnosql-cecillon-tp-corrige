# Exercice 3 du TD1 de MIF24 BD NoSQL

Le code présenté ici est en Javascript. Le fichier
[exo3-interpreteur.js](exo3-interpreteur.js) contient le code de l'interpréteur.
Le fichier [test/test-exo3-interpreteur.js](test/test-exo3-interpreteur.js)
contient un certain nombre de tests pour le code du fichier précédent.

Pour lancer les tests:

```
npm install # à faire une seule fois, installe l'outil de test mocha
npm run test
```

Le code s'appuie sur une représentation JSON des requêtes JSON Path telle que
décrite dans le question 1 (c.f. [corrigé](../mif24-td-01-chemins.pdf)).

Le code après le commentaire `// Builder d'expressions` n'est pas utile pour
l'interpréteur, mais il permet de construire plus facilement des structures JSON
de représentation des expression JSON Path. En particulier, les tests utilisent
ce code pour fabriquer des expressions à tester.
